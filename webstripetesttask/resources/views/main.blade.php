@extends('wrappers.main')

@section('content')
<style media="screen">

</style>

<div class="container mb-5">
  <div id="modal_sended_form" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-body">
          <p>Data from form has been sended</p>
        </div>
      </div>

    </div>
  </div>
  <div class="row m-4">
    <p class="text-right col-12">Name Omiron surname Deykun</p>
  </div>
  <div class="row m-5">
    <h4 class="text-center col-12 text-uppercase">Webstripe test</h4>
  </div>

  <div class="row m-5">
    <!-- Top content -->
<div class="top-content">
    <div class="container-fluid">
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner row w-100 mx-auto" role="listbox">
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
                    <img src="https://scontent-arn2-2.cdninstagram.com/v/t51.2885-15/e35/44456694_268624977129296_2375744345501525626_n.jpg?_nc_ht=scontent-arn2-2.cdninstagram.com&_nc_cat=108&_nc_ohc=HOP5m8hUNPUAX9-zBRb&oh=daebcab203859aa73f4003fb39f52d5a&oe=5EE23150" class="img-fluid mx-auto d-block" alt="img1">
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                    <img src="https://scontent-arn2-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s750x750/44189218_286781495511205_4996992115573589424_n.jpg?_nc_ht=scontent-arn2-1.cdninstagram.com&_nc_cat=101&_nc_ohc=rzINgu8MqHAAX-9g-YN&oh=05d517cba3e5cec7d1c417f413556815&oe=5EE32C1F" class="img-fluid mx-auto d-block" alt="img2">
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                    <img src="https://scontent-arn2-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s750x750/45493016_138383270472575_3413528900122553152_n.jpg?_nc_ht=scontent-arn2-1.cdninstagram.com&_nc_cat=111&_nc_ohc=zHdJ3Hj-I9YAX8fqvE2&oh=9ce98460b7649088f275d9df7b35ae1c&oe=5EE00CA3" class="img-fluid mx-auto d-block" alt="img3">
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                    <img src="https://scontent-arn2-1.cdninstagram.com/v/t51.2885-15/e35/51226445_385583958938758_4124171221060616685_n.jpg?_nc_ht=scontent-arn2-1.cdninstagram.com&_nc_cat=110&_nc_ohc=pq2OFrNtj0IAX8-mshv&oh=172d21116d63cb754046c3ba66214249&oe=5EE0C2A4" class="img-fluid mx-auto d-block" alt="img4">
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                    <img src="https://scontent-arn2-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s750x750/54226384_1950227421772876_8351826763623483991_n.jpg?_nc_ht=scontent-arn2-1.cdninstagram.com&_nc_cat=104&_nc_ohc=txTzT0Yay0AAX-Eg1aj&oh=a91019ece94885fa8be022eb134a1ff9&oe=5EE0D641" class="img-fluid mx-auto d-block" alt="img5">
                </div>
                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                    <img src="https://scontent-arn2-2.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/53848952_410627046400472_5149098460212551747_n.jpg?_nc_ht=scontent-arn2-2.cdninstagram.com&_nc_cat=108&_nc_ohc=_RqxOoaFagUAX_mGvkK&oh=746a597efacc1b94f239f5a6c5b6c9f5&oe=5EE203E3" class="img-fluid mx-auto d-block" alt="img6">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
  </div>

  <div class="row">
    <div class="col-md-6 col-sm-12">
      <form action="{{ route('form') }}" method="post" id="form">
        @csrf
        <div class="form-group">
          <label for="exampleFormControlInput1">Name</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="name"  required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlInput1">Email address</label>
          <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlInput1">Any text</label>
          <input type="text" class="form-control" id="text" name="text" placeholder="text" required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlInput1">First Number</label>
          <input type="number" class="form-control" id="first" name="first" placeholder="1" required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlInput1">Second Number</label>
          <input type="number" class="form-control" id="second" name="second" placeholder="2" required>
        </div>

        <div id="newRow"></div>
        <button id="addRow" type="button" class="btn btn-secondary btn-sm">Add file input</button>


          <button type="submit" name="button" id="send_button" class="btn btn-dark">Send</button>

      </form>

    </div>
    <div class="col-md-6 col-sm-12">
      <button type="button" id="display_button" class="btn btn-dark">display</button>

      <p>Name: <span id="value_name">Click "display" to view value</span></p>
      <p>Email: <span id="value_email">Click "display" to view value</span></p>
      <p>Text: <span id="value_text">Click "display" to view value</span></p>
      <p>First number: <span id="value_first">Click "display" to view value</span></p>
      <p>Second number: <span id="value_second">Click "display" to view value</span></p>
      <p>Sum of number: <span id="value_sum">Click "display" to view value</span></p>
      <p>Every second letter: <span id="value_second_letter">Click "display" to view value</span></p>
      <p>Length of the first element in words : <span id="value_length">Click "display"...</span></p>
      <div id="value_sum_by_number"></div>
    </div>
  </div>
</div>

<div class="row">
  <iframe src="https://yandex.com/map-widget/v1/?um=constructor%3Af831b143c238437e09598cb02c33577dbe1a255533bb3a42a5c3df2bb6b262fd&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
</div>

@endsection
