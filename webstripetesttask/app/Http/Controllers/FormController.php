<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormData;
use Illuminate\Support\Facades\DB;
use App\Exports\FormDataExport;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use NumberFormatter;



class FormController extends Controller
{
    public function worker(Request $r) {
      $fd = new FormData();
      $fd->name = $r->input('name');
      $fd->email = $r->input('email');
      $fd->text = $r->input('text');
      $fd->first = $r->input('first');
      $fd->second = $r->input('second');
      $fd->sum = $r->input('first') + $r->input('second');

      DB::delete('delete from form_data');

      $fd->save();

      Excel::store(new FormDataExport, 'data.xlsx', 'local');

      $to_name="name";
      $to_email="omirondeykun@mail.ru";
      $data=array('name' => $fd->name);
      Mail::send(
        'mail',
        $data,
        function($msg) use ($to_name, $to_email, $r){
          $storage_path = str_replace('\\', '/', storage_path());

          $msg->attach($storage_path.'/app/data.xlsx');

          for ($i=0; $i < $r->count_of_file; $i++) {
            $filename = 'file';
            $filename .= $i;
            if ($r->hasFile($filename)) {
                $filepath = $r->$filename->store('files');
                $msg->attach($storage_path.'/app/'.$filepath);

            }
          }
          $msg->to($to_email)->subject('Your files, ser');
        }
      );

      return "saved";
    }

    public function form_data() {
      $result = array();

      // $table_data = DB::table('form_data')->first();
      $table_data = DB::table('form_data')->first();

      error_log($table_data->id);

      $every_second_res = '';
      $every_second = array($table_data->name, $table_data->email, $table_data->text);
      foreach ($every_second as $value) {
        $value = preg_replace('/[^a-zA-Z0-9-]/', '_', $value);
        for ($i=0; $i < strlen($value); $i++) {
          if($i % 2 == 0){
            $every_second_res.=$value[$i];
          }
        }
      }

      $number_array = array($table_data->first, $table_data->second, $table_data->sum);

      $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT);

      $result[] = DB::table('form_data')->get();
      $result[] = $every_second_res;
      $result[] = $digit->format(strlen($every_second[1]));
      $result[] = array_sum($number_array);

      return $result;
    }

}
