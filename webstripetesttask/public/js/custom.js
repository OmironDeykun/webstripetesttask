$(document).ready(function(){

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  var count_of_file = 0;

    $("#addRow").click(function () {
          var html = '';
          html +='<div class="form-group file_input" >';
          html +='<label for="exampleFormControlFile1">Example file input</label>';
          html +='<input type="file" name="file'+count_of_file+'" class="form-control-file" id="exampleFormControlFile1">';
          html +='<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
          html +='</div>';
          $('#newRow').append(html);
          count_of_file++;
      });

      $(document).on('click', '#removeRow', function () {
            $(this).closest('.file_input').remove();
            count_of_file--;
        });



    $('#form').on('submit', function(e) {

        e.preventDefault();

        var button = $('#send_button');
        button.remove();
        $('#form').append('<div class="spinner-border ml-auto" role="status" aria-hidden="true" id="spinner"></div>');


        var data = new FormData(document.getElementById('form'));
        data.append('count_of_file', count_of_file);

        $("#name").val("");
        $("#email").val("");
        $("#text").val("");
        $("#first").val("");
        $("#second").val("");

        for (const child of document.getElementsByClassName('file_input')) {
          child.remove();
        }

        $.ajax({
           type:'POST',
           url:'/',
           data: data,
           contentType: false,
            processData:false,

           success:function(data){
              if (data == "saved") {
                $('#modal_sended_form').modal('show');

                $('#spinner').remove();
                $('#form').append(button);

                setTimeout(function () {
                  $('#modal_sended_form').modal('hide')
                }, 1000);

              }
           }
        });



	});

  $("#display_button").click(function () {
    $.ajax({
       type:'get',
       url:'/form_data',

       success:function(data){
          console.log(data);
          $("#value_name").text(data[0][0]['name']);
          $("#value_email").text(data[0][0]['email']);
          $("#value_text").text(data[0][0]['text']);
          $("#value_first").text(data[0][0]['first']);
          $("#value_second").text(data[0][0]['second']);
          $("#value_sum").text(data[0][0]['sum']);
          $("#value_second_letter").text(data[1]);
          $("#value_length").text(data[2]);
          data[3] = data[3].toString();
          for (var i = 0; i < data[3].length; i++) {
            $('#value_sum_by_number').append('<p>'+data[3][i]+'</p>');
          }
       }
    });
  });

  /*
    Carousel
*/
$('#carousel-example').on('slide.bs.carousel', function (e) {
    /*
        CC 2.0 License Iatek LLC 2018 - Attribution required
    */
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});
});
